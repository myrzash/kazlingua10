import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kazlingua10/models/DataProvider.dart';
import 'package:kazlingua10/models/MainModel.dart';
import 'package:kazlingua10/models/Part.dart';
import 'package:kazlingua10/views/MainMenu.dart';
import 'package:kazlingua10/views/PartCard.dart';
import 'package:scoped_model/scoped_model.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List<Part> parts = [];
  bool isLoading = true;

  void fetchParts() async {
    List<Part> parts = await DataProvider().fetchParts(context);
    setState(() {
      this.parts = parts;
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    isLoading = true;
    fetchParts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Colors.blue.shade600,
                const Color(0xFF009ed6),
                Colors.blue.shade600,
              ],
            ),
          ),
        ),
        backgroundColor: const Color(0xFF009ed6),
        title: Text('ҚазLingua'),
        centerTitle: true,
      ),
      drawer: MainMenu(),
      body: isLoading
          ? Center(
              child: SpinKitFadingCube(
                color: const Color(0xFF009ed6),
                size: 50.0,
              ),
            )
          : GridView.count(
              padding: EdgeInsets.all(5),
              crossAxisCount: 2,
              childAspectRatio: (160 / 210),
              children: parts
                  .map((part) => ScopedModelDescendant<MainModel>(
                        builder: (context, _, model) => PartCard(
                          part: part,
                          onItemClick: () {
                            model.selectPart(part);
                            Navigator.pushNamed(context, '/part');
                          },
                        ),
                      ))
                  .toList(),
            ),
    );
  }
}
