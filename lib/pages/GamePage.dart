import 'package:flutter/material.dart';
import 'package:flutter_rounded_progress_bar/flutter_rounded_progress_bar.dart';
import 'package:flutter_rounded_progress_bar/rounded_progress_bar_style.dart';
import 'package:kazlingua10/fragments/ResultGameFragment.dart';
import 'package:kazlingua10/helpers/GameGenerator.dart';
import 'package:kazlingua10/models/GameState.dart';
import 'package:kazlingua10/models/MainModel.dart';

class GamePage extends StatefulWidget {
  @override
  _GamePageState createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {
  GameGenerator generator;
  Widget childView;
  int position = 0;
  int prev = -1;
  int progress = 0;

  @override
  Widget build(BuildContext context) {
    if (generator == null) {
      var unit = MainModel.of(context).currentUnit;
      generator = GameGenerator(unit: unit);
    }

    if (position > prev) {
      childView = getChildView();
      prev++;
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      // resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.grey[200],
      body: SafeArea(
          child: Column(
        children: <Widget>[
          SizedBox(height: 5),
          Row(
            children: <Widget>[
              BackButton(
                color: Colors.blueGrey[700],
              ),
              Expanded(
                flex: 1,
                child: RoundedProgressBar(
                  height: 24,
                  milliseconds: 1000,
                  percent: generator.getPercentage(position: position),
                  theme: RoundedProgressBarTheme.yellow,
                  borderRadius: BorderRadius.circular(0),
                ),
              ),
              SizedBox(width: 10),
              Icon(
                Icons.star,
                color: Colors.amber,
              ),
              Text(
                '$progress',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: Colors.blueGrey[700],
                ),
              ),
              SizedBox(width: 10),
            ],
          ),
          Expanded(
            flex: 1,
            child: childView,
          )
        ],
      )),
    );
  }

  Widget getChildView() {
    if (generator.isGame(position: position)) {
      return GameState(
        onCorrect: () {
          setState(() {
            progress++;
          });
        },
        onNext: () {
          setState(() {
            position++;
          });
        },
        child: generator.getCurrentGame(position: position),
      );
    }

    int result = (100 * progress / position).round();
    return ResultGameFragment(percent: result);
  }
}
