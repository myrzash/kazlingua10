import 'package:flutter/material.dart';

List<Color> colors = [
  Colors.amber[700],
  Colors.lightBlueAccent,
  Colors.pink[300],
  Colors.tealAccent[700],
  Colors.red[300],
  Colors.blue[500],
  Colors.green[400],
  Colors.deepPurple[300],
];
