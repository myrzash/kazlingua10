import 'package:flutter/material.dart';
import 'package:kazlingua10/models/Part.dart';
import 'package:kazlingua10/models/Unit.dart';
import 'package:scoped_model/scoped_model.dart';

class MainModel extends Model {
  Part _currentPart;
  Unit _currentUnit;

  Part get currentPart => _currentPart;

  Unit get currentUnit => _currentUnit;

  void selectPart(Part part) {
    _currentPart = part;
    notifyListeners();
  }

  void selectUnit(Unit unit) {
    _currentUnit = unit;
    notifyListeners();
  }

  static MainModel of(BuildContext context) =>
      ScopedModel.of<MainModel>(context, rebuildOnChange: true);
}
