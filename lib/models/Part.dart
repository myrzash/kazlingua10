import 'package:flutter/material.dart';
import 'package:kazlingua10/models/Unit.dart';

class Part {
  String name;
  String image;
  Color bgColor;
  List<Unit> units;

  Part({this.name, this.image, this.bgColor});

  Part.fromJSON(json) {
    name = json["name"];
    image = json["image"];
    List units = json["contents"];
    this.units = units == null
        ? []
        : units.map((json) => Unit.fromJSON(json, name)).toList();
  }
}
