class Sort {
  String audio;
  List<String> sentences;

  Sort({this.audio, this.sentences});

  Sort.fromJSON(json) {
    audio = json['audio'];
    List sentences = json['sentences'];
    this.sentences = sentences == null
        ? []
        : sentences
            .map((json) =>
                json.toString().trim().replaceAll(new RegExp(r"\s+"), " "))
            .toList();
  }
}
