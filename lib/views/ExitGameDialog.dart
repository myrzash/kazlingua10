import 'package:flutter/material.dart';

class ExitGameDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text(
          'Вы уверены?',
          textAlign: TextAlign.center,
        ),
        content: Text(
          'Весь прогресс данного урока будет потерян',
          textAlign: TextAlign.center,
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.of(context).pop(true);
            },
            child: Text('Да'),
          ),
          TextButton(
            onPressed: () {
              Navigator.of(context).pop(false);
            },
            child: Text('Нет'),
          )
        ]);
  }
}
