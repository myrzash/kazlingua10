import 'package:flutter/material.dart';
import 'package:kazlingua10/fragments/BaseGameFragment.dart';

class SentenceFragment extends GameFragment {
  final String task;
  final String sentence;

  SentenceFragment({Key key, @required this.task, @required this.sentence})
      : super(key: key);

  @override
  _SentenceFragmentState createState() => _SentenceFragmentState();
}

class _SentenceFragmentState extends GameFragmentState<SentenceFragment>
    with BasicGame {
  List<String> primaryWords;
  List<String> words;
  List<String> basket = [];

  @override
  void initState() {
    super.initState();

    primaryWords = widget.sentence.split(' ');
    words = widget.sentence.split(' ')..shuffle();
  }

  @override
  String get task => 'Сұраққа жауап бер';

  @override
  bool get activeCheck => words.length == 0;

  @override
  bool get isCorrect =>
      widget.sentence.toLowerCase() == basket.join(' ').toLowerCase();

  @override
  bool get activeNext => isCorrect;

  @override
  Widget body() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            '${widget.task}',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w700,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
            flex: 1,
            child: Card(
              elevation: 0,
              child: Container(
                padding: EdgeInsets.all(15),
                child: Wrap(
                  spacing: 5,
                  runSpacing: 0,
                  children: basket
                      .asMap()
                      .map((index, item) => MapEntry(
                          index,
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              elevation: 1,
                              primary: Colors.grey.shade300,
                            ),
                            onPressed: () => checked
                                ? null
                                : setState(() {
                                    words.add(item);
                                    basket.remove(item);
                                  }),
                            child: Text(
                              '$item',
                              style: TextStyle(
                                fontSize: 16,
                                color: getColorText(item, index),
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )))
                      .values
                      .toList(),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 0,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              child: Wrap(
                alignment: WrapAlignment.center,
                spacing: 5,
                runSpacing: 0,
                children: words
                    .map((item) => ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            elevation: 1,
                            primary: Colors.white,
                            onPrimary: Colors.grey.shade800,
                          ),
                          onPressed: () => setState(() {
                            basket.add(item);
                            words.remove(item);
                          }),
                          child: Text(
                            '$item',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ))
                    .toList(),
              ),
            ),
          ),
        ]);
  }

  @override
  void onWrong() {
    Future.delayed(Duration(milliseconds: 1500), () {
      setState(() {
        basket = primaryWords;
      });
    });
  }

  Color getColorText(item, index) {
    if (!checked) return Colors.black;

    bool isCorrect = primaryWords[index] == item;

    return isCorrect ? Colors.green[400] : Colors.red[400];
  }
}
