import 'package:flutter/material.dart';
import 'package:kazlingua10/helpers/SoundManager.dart';
import 'package:kazlingua10/models/GameState.dart';
import 'package:kazlingua10/views/ExitGameDialog.dart';
import 'package:kazlingua10/views/RoundedButton.dart';

abstract class GameFragment extends StatefulWidget {
  GameFragment({Key key}) : super(key: key);
}

abstract class GameFragmentState<Page extends GameFragment>
    extends State<Page> {
  String get task;

  bool get isCorrect;

  bool get activeCheck;

  bool get activeNext;

  bool checked = false;

  bool autoNext = false;
}

mixin BasicGame<Page extends GameFragment> on GameFragmentState<Page> {
  @override
  Widget build(BuildContext context) {
    Function onNext = GameState.of(context).onNext;

    return WillPopScope(
      onWillPop: () => showExitDialog(),
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              task,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18,
              ),
            ),
            SizedBox(height: 5),
            Expanded(
              flex: 1,
              child: body(),
            ),
            checked
                ? RoundedButton(
                    text: 'Дальше',
                    onPressed: activeNext && !autoNext ? onNext : null,
                  )
                : RoundedButton(
                    text: 'Проверить',
                    onPressed: activeCheck ? check : null,
                  ),
          ],
        ),
      ),
    );
  }

  Widget body();

  void onWrong();

  Future<bool> showExitDialog() {
    return showDialog(context: context, builder: (context) => ExitGameDialog());
  }

  void check() {
    if (!activeCheck) return;

    setState(() {
      checked = true;
    });

    if (isCorrect) {
      SoundManager.shared.playCorrect();
      Function onCorrect = GameState.of(context).onCorrect;
      Function onNext = GameState.of(context).onNext;
      onCorrect();
      autoNext = true;
      Future.delayed(Duration(milliseconds: 800), () {
        onNext();
        autoNext = false;
      });
    } else {
      SoundManager.shared.playError();
      onWrong();
    }
  }
}
